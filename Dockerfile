FROM node:12-alpine
LABEL maintainer="Grigor Mouradyan <grigor.mouradyan@eemi.com>"
EXPOSE 3000

RUN ["addgroup", "-S", "engagement"]
RUN ["adduser", "-S", "-D", "-h", "/home/engagement", "-G", "engagement", "engagement"]

USER engagement
WORKDIR /home/engagement

COPY --chown=engagement:engagement ./package-lock.json ./package.json ./
RUN ["npm", "ci", "--only=prod"]
COPY --chown=engagement:engagement ./src ./src
COPY --chown=engagement:engagement ./bin ./bin

ENTRYPOINT ["/usr/local/bin/node"]
CMD ["./bin/www"]
