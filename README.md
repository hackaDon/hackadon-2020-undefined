# Engament API

**Engament API** is a Node.js and MongoDB-based service. It exports a REST-over-HTTP
API that can be used to manage (create, retrieve, update, etc.) users, messages, competencies, etc...,
in a customer-agnostic way.

## Development

To install **Engament API** in a prod environment:

```sh
export COMMENTS_HOME=~/projects/comments # choose a suitable directory to checkout the codebase
cd $COMMENTS_HOME
git clone https://gitlab.insideboard.com/microservices/comments.git .
npm install
npm start
```

This will create two containers : mdb for MongoDB and api for the api.

To install **Engament API** in a development environment:

```sh
export COMMENTS_HOME=~/projects/comments # choose a suitable directory to checkout the codebase
cd $COMMENTS_HOME
git clone https://gitlab.insideboard.com/microservices/comments.git .
npm install
npm run dev
```

Application logs are written to `stdout` as a JSON stream. These logs can be
pretty-printed during development:

```sh
npm run dev | ./node_modules/.bin/pino-pretty
```

## API Documentation

The REST API spec is located under **/docs** and is written using [OpenAPI 3.0](https://www.openapis.org).
HTML documentation can be built and served locally using Docker:

```sh
npm run docs-build
npm run docs-serve
```

### Healthcheck

The service exposes a healthcheck endpoint available via HTTP GET at `/status`.
It always returns with HTTP 200, as long as the service is up:

```
GET /status HTTP/1.1
```

```
HTTP/1.1 200 Ok
```

## Configuration

**Engament API** is configured by the following environment variables:

Variable | Description | Allowed Values | Default
--- | --- | --- | ---
**TOKEN_SECRET** | The secret for JWT auth strategy. | Any string. | `6_eZb6EJ_i$nxtT`
**DB_CONNECTION_STRING_TEMPLATE** | A template string used to dynamically establish database connections. | Any string. | `mongodb://localhost:27017`
