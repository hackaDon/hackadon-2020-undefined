const jwt = require('jsonwebtoken');
const HTTP_CODE = require('http-status-codes');

const verifyAuth = (req, res, next) => {
  const token = req.header('auth');
  if (!token) return res.status(HTTP_CODE.UNAUTHORIZED).send('Access Denied.');

  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = verified;
    next();
  } catch (error) {
    res.status(HTTP_CODE.BAD_REQUEST).send('Invalid Token.');
    return error;
  }
};

module.exports = {
  verifyAuth,
};
