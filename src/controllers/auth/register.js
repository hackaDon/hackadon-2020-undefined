const HTTP_CODE = require('http-status-codes');
const { User, joiUserSchema } = require('../../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const register = async (req, res) => {
  try {
    const { error } = joiUserSchema.validate(req.body, { abortEarly: false });

    if (error) return res.status(HTTP_CODE.BAD_REQUEST).json({ error });

    const userExists = await User.findOne({ email: req.body.email });

    if (userExists)
      return res
        .status(HTTP_CODE.BAD_REQUEST)
        .json({ message: 'Email already used.' });

    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt);

    const user = new User({
      name: req.body.name,
      email: req.body.email,
      role: req.body.role,
      phone: req.body.phone,
      password: hashPassword,
      address: req.body.address,
    });

    try {
      await user.save();
      const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);

      res.status(HTTP_CODE.CREATED).json({ auth: token });
    } catch (error) {
      res.status(HTTP_CODE.BAD_REQUEST).send(error);
    }
  } catch (error) {
    res.sendStatus(HTTP_CODE.INTERNAL_SERVER_ERROR);
    return error;
  }
};

module.exports = register;
