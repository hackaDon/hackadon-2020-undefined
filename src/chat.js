const app = require('express')(),
  server = require('http').createServer(app),
  io = require('socket.io').listen(server);

app.get('/', (req, res) => {
  res.sendfile(__dirname + '/chat.html');
});

io.sockets.on('connection', (socket, pseudo) => {
  socket.on('nouveau_client', pseudo => {
    socket.pseudo = pseudo;
    socket.broadcast.emit('nouveau_client', pseudo);
  });

  socket.on('message', message => {
    socket.broadcast.emit('message', {
      pseudo: socket.pseudo,
      message: message,
    });
  });
});

server.listen(3003, () => {
  console.log('ok');
});
