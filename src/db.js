const mongoose = require('mongoose');

const getDbConnection = async dbConnectionStringTemplate => {
  try {
    dbConnection = await mongoose.connect(
      dbConnectionStringTemplate,
      {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useCreateIndex: true,
        dbName: 'engagement',
      },
      error => {
        if (error) {
          console.error(error);
          return process.exit(1);
        }
        console.log('Connected to db.');
      },
    );

    return dbConnection;
  } catch (error) {
    console.error(error);
    return process.exit(1);
  }
};

module.exports = {
  getDbConnection,
};
