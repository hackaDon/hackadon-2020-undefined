const mongoose = require('mongoose');
const joigoose = require('joigoose')(mongoose);
const joi = require('@hapi/joi');

const joiLoginSchema = joi.object({
  email: joi
    .string()
    .email()
    .required(),
  password: joi
    .string()
    .min(8)
    .max(255)
    .required(),
});

const joiUserSchema = joi.object({
  name: joi
    .object({
      first: joi.string().required(),
      last: joi.string().required(),
    })
    .required(),
  role: joi
    .object({
      type: joi.string().required(),
    })
    .required(),
  email: joi
    .string()
    .email()
    .required(),
  password: joi
    .string()
    .min(8)
    .max(255)
    .required(),
  phone: joi
    .string()
    .regex(/^[0-9]{10}$/)
    .required(),
  address: joi
    .object({
      street: joi.string().required(),
      zip: joi
        .string()
        .regex(/^[0-9]{5}$/)
        .required(),
      city: joi
        .string()
        .regex(/^[a-zA-Z]+([-' ]?[a-zA-Z])*$/)
        .required(),
      country: joi
        .string()
        .regex(/^[a-zA-Z]+$/)
        .required(),
    })
    .required(),
});

const userSchema = new mongoose.Schema(joigoose.convert(joiUserSchema), {
  timestamps: true,
});
const User = mongoose.model('User', userSchema);

module.exports = {
  joiUserSchema,
  joiLoginSchema,
  User,
};
